import 'package:game/game.dart' as game;
import 'dart:io';

class Player {
  int index = 0;
  int hp = 2;
  int countAnimal = 0;
  int countThing = 0;
  int countSport = 0;
  int countHuman = 0;

  int score = 0;
  void walk() {
    index++;
  }

  void jump() {
    index += 2;
  }
}

class Game {
  Map map = Map();
  Player player = Player();
  int count = 1;

  void process() {
    while (true) {
      print("HP: ${player.hp}");
      map.showMap();
      String input = stdin.readLineSync()!;
      if (input == "1") {
        if (player.index < 40) {
          if (map.isTree(player.index + 1)) {
            continue;
          }
          player.walk();
        }
        map.setPlayer(player.index);
      } else if (input == "J") {
        if (player.index < 39) {
          if (map.isTree(player.index + 2)) {
            continue;
          }
          player.jump();
        }
        map.setPlayerJ(player.index);
      }
      if (map.isHeart(player.index)) {
        print("HP++");
        player.hp++;
      }
      if (map.isQuestion(player.index)) {
        print("A.Animal  B.Thing   C.Sport   D.Human");
        input = stdin.readLineSync()!;
        if (input == "A") {
          Animal question = Animal("A", player.index);
          question.showQuestion(player);
        }
        if (input == "B") {
          Thing question = Thing("T", player.index);
          question.showQuestion(player);
        }
        if (input == "C") {
          Sport question = Sport("S", player.index);
          question.showQuestion(player);
        }
        if (input == "D") {
          Human question = Human("H", player.index);
          question.showQuestion(player);
        }
      }
      if (isFinish()) {
        if (player.index >= 40) {
          print("You Win!!!!");
        } else {
          print("You Lose T_T");
        }
        print("Score:${player.score}");
        break;
      }
    }
  }

  bool isFinish() {
    if (player.index == 40 || player.hp == 0) return true;
    return false;
  }
}

class Map {
  List<String> map = ["P"];
  Map() {
    for (int i = 1; i <= 40; i++) {
      if (i % 5 == 0) {
        map.add("Q");
      } else if (i == 12) {
        map.add("H");
      } else if (i % 3 == 0) {
        map.add("T");
      } else {
        map.add("-");
      }
    }
  }
  bool isQuestion(int index) {
    if (index % 5 == 0) {
      return true;
    }
    return false;
  }

  bool isTree(int index) {
    if (index % 3 == 0 && index != 15 && index != 12 && index != 30) {
      return true;
    }
    return false;
  }

  void showMap() {
    print(map);
  }

  void setPlayer(int index) {
    map[index - 1] = "-";
    map[index] = "P";
  }

  void setPlayerJ(int index) {
    map[index - 2] = "-";
    map[index] = "P";
  }

  bool isHeart(int index) {
    if (index == 12) {
      return true;
    }
    return false;
  }
}

class Object {
  late int x;
  late String symbol;

  Object(String symbol, int x) {
    this.symbol = symbol;
    this.x = x;
  }
}

class Tree extends Object {
  Tree(super.symbol, super.x);
}

class Heart extends Object {
  Heart(super.symbol, super.x);
}

class Question extends Object {
  String type = "";

  Question(super.symbol, super.x);

  void showQuestion(Player player) {
    String answer = "";

    if (type == "Animal") {
      player.countAnimal++;
      if (player.countAnimal == 1) {
        print("It has a very big horn.");
        print("It is the national animal of Sweden.");
        print("A.hen B.moose C.cow");
      }
      if (player.countAnimal == 2) {
        print("It's a large bird.");
        print("Its eggs are big, but they can't fly.");
        print("A.ostrich B.eagle C.seagull");
      }
      if (player.countAnimal == 3) {
        print("It is an animal that lives in the desert.");
        print("Its back was like two mountains.");
        print("A.cat B.dog C.camel");
      }
      if (player.countAnimal == 4) {
        print("it's a reptile.");
        print("It moves by slithering.");
        print("A.snake B.iguana C.bird");
      }
      if (player.countAnimal == 5) {
        print("It likes to jump on lotus leaves.");
        print("It likes to make noise Aob Aob.");
        print("A.frog B.bird C.cat");
      }
      if (player.countAnimal == 6) {
        print("It likes to eat bamboo.");
        print("It has white and black fur.");
        print("A.zebra B.panda C.bee");
      }
      if (player.countAnimal == 7) {
        print("It likes to eat honey.");
        print("It was large with brown fur.");
        print("A.bean B.bee C.bear");
      }
      if (player.countAnimal == 8) {
        print("It likes to eat the blood of people and animals.");
        print("It's not a vampire");
        print("A.vampire B.cockroach C.mosquito");
      }
      answer = stdin.readLineSync()!;
    } else if (type == "Thing") {
      player.countThing++;
      //-------------------------------------------------------------------------------------------------
      if (player.countThing == 1) {
        print("My father used it to mow the grass in the backyard.");
        print("It shortens the grass in the backyard.");
        print("A.lawn mower B.buffalo C.microwave");
      }
      if (player.countThing == 2) {
        print("I always use it when I'm done taking a shower.");
        print("It makes your body dry after taking a shower.");
        print("A.soap B.water C.towel");
      }
      if (player.countThing == 3) {
        print("My mother used it to sweep the floor.");
        print("The witch uses it as a vehicle.");
        print("A.mop B.broom C.airplane");
      }
      if (player.countThing == 4) {
        print("My sister likes to use it when she draws.");
        print("It has beautiful colors.");
        print("A.crayon B.duck C.table");
      }
      if (player.countThing == 5) {
        print("My brother uses this to make sweets.");
        print("It's a cylindrical stick.");
        print("A.bread stick B.sugar C.baseball bat");
      }
      if (player.countThing == 6) {
        print("I use it when it's cold.");
        print("It is very important in winter.");
        print("A.bonfire B.Heater C.fan");
      }
      if (player.countThing == 7) {
        print("My father used it in my mother's flower garden.");
        print("It is used to cut branches.");
        print("A.broom B.mop C.pruning shears");
      }
      if (player.countThing == 8) {
        print("My sister uses it to watch TV.");
        print("it is placed in front of the TV.");
        print("A.sofa B.pillow C.curtain");
      }
      answer = stdin.readLineSync()!;
    } else if (type == "Sport") {
      player.countSport++;
      //-------------------------------------------------------------------------------------------------
      if (player.countSport == 1) {
        print("Athletes use their foot to play.");
        print("This sport uses 11 players.");
        print("A.tennis B.football C.muzzle");
      }
      if (player.countSport == 2) {
        print("LeBron Raymone James plays this sport.");
        print("to throw the ball into the hoop.");
        print("A.handball B.basketball C.badminton");
      }
      if (player.countSport == 3) {
        print("This sport must be played in the pool.");
        print("Those who can swim can play this sport.");
        print("A.swimming sports B.table tennis C.football");
      }
      if (player.countSport == 4) {
        print("This sport uses shuttlecocks to play.");
        print("There is a net to block both sides.");
        print("A.basketball B.table tennis C.badminton");
      }
      if (player.countSport == 5) {
        print("This sport only requires five players.");
        print("This sport is played like football.");
        print("A.football B.Futsal C.table tennis");
      }
      if (player.countSport == 6) {
        print("An athlete named Buakaw plays this sport.");
        print("It is a sport that uses martial arts.");
        print("A.Thai dance B.boxing C.aerobic dance");
      }
      if (player.countSport == 7) {
        print("This sport requires a steel pendulum.");
        print("This sport uses a lot of arm strength.");
        print("A.Shot put B.thai boxing C.tai chi");
      }
      if (player.countSport == 8) {
        print("This sport uses bicycles to race.");
        print("Competitors have to ride a bicycle for several kilometers.");
        print("A.long jump B.swim C.cycling");
      }
      answer = stdin.readLineSync()!;
    } else if (type == "Human") {
      player.countHuman++;
      //-------------------------------------------------------------------------------------------------
      if (player.countHuman == 1) {
        print("We use this section to look at things around us.");
        print("It comes in a twin pack.");
        print("A.hand B.leg C.eye");
      }
      if (player.countHuman == 2) {
        print("It is part of the respiratory system.");
        print("It was on the face and had two holes.");
        print("A.eye B.nose C.ear");
      }
      if (player.countHuman == 3) {
        print("It is an important organ of the body.");
        print("it works 24 hours");
        print("A.head B.arm C.heart");
      }
      if (player.countHuman == 4) {
        print("It is part of the respiratory system.");
        print("It is often destroyed by cigarette smoke.");
        print("A.lungs B.lips C.tooth");
      }
      if (player.countHuman == 5) {
        print("It serves to cover the head.");
        print("There are different colored lines according to genetics.");
        print("A.hair B.tooth C.pedicure");
      }
      if (player.countHuman == 6) {
        print("It is responsible for the various systems of the body.");
        print("it's in our head");
        print("A.brain B.kidney C.knee");
      }
      if (player.countHuman == 7) {
        print("It is responsible for transporting food to the stomach.");
        print("It is very long.");
        print("A.large intestine B.hair C.nail");
      }
      if (player.countHuman == 8) {
        print("It's an organ that only men can see.");
        print("it's on the neck");
        print("A.arm B.Adam's Apple C.head");
      }
      answer = stdin.readLineSync()!;
    } else {
      answer = "";
    }
    checkAnswer(answer, player);
  }

  void checkAnswer(String answer, Player player) {}
}

//-------------------------------------------------------------------------------------------------
class Animal extends Question {
  String type = "Animal";

  Animal(super.symbol, super.x) {
    this.symbol = "A";
  }
  void checkAnswer(String answer, Player player) {
    if (player.countAnimal == 1) {
      if (answer == "B") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
    if (player.countAnimal == 2) {
      if (answer == "A") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
    if (player.countAnimal == 3) {
      if (answer == "C") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
    if (player.countAnimal == 4) {
      if (answer == "A") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
    if (player.countAnimal == 5) {
      if (answer == "A") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
    if (player.countAnimal == 6) {
      if (answer == "B") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
    if (player.countAnimal == 7) {
      if (answer == "C") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
    if (player.countAnimal == 8) {
      if (answer == "C") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
  }
}

//-------------------------------------------------------------------------------------------------
class Thing extends Question {
  String type = "Thing";

  Thing(super.symbol, super.x) {
    this.symbol = "T";
  }
  void checkAnswer(String answer, Player player) {
    if (player.countThing == 1) {
      if (answer == "A") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
    if (player.countThing == 2) {
      if (answer == "C") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
    if (player.countThing == 3) {
      if (answer == "B") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
    if (player.countThing == 4) {
      if (answer == "A") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
    if (player.countThing == 5) {
      if (answer == "A") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
    if (player.countThing == 6) {
      if (answer == "B") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
    if (player.countThing == 7) {
      if (answer == "C") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
    if (player.countThing == 8) {
      if (answer == "A") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
  }
}

//-------------------------------------------------------------------------------------------------
class Sport extends Question {
  String type = "Sport";

  Sport(super.symbol, super.x) {
    this.symbol = "S";
  }
  void checkAnswer(String answer, Player player) {
    if (player.countSport == 1) {
      if (answer == "B") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
    if (player.countSport == 2) {
      if (answer == "B") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
    if (player.countSport == 3) {
      if (answer == "A") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
    if (player.countSport == 4) {
      if (answer == "C") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
    if (player.countSport == 5) {
      if (answer == "B") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
    if (player.countSport == 6) {
      if (answer == "B") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
    if (player.countSport == 7) {
      if (answer == "A") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
    if (player.countSport == 8) {
      if (answer == "C") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
  }
}

//-------------------------------------------------------------------------------------------------
class Human extends Question {
  String type = "Human";

  Human(super.symbol, super.x) {
    this.symbol = "H";
  }
  void checkAnswer(String answer, Player player) {
    if (player.countHuman == 1) {
      if (answer == "C") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
    if (player.countHuman == 2) {
      if (answer == "B") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
    if (player.countHuman == 3) {
      if (answer == "C") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
    if (player.countHuman == 4) {
      if (answer == "A") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
    if (player.countHuman == 5) {
      if (answer == "A") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
    if (player.countHuman == 6) {
      if (answer == "A") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
    if (player.countHuman == 7) {
      if (answer == "A") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
    if (player.countHuman == 8) {
      if (answer == "B") {
        print("Correct!!!");
        player.score++;
      } else {
        print("Wrong Answer");
        player.hp--;
      }
    }
  }
}

void main(List<String> arguments) {
  Game game = Game();
  game.process();
}
